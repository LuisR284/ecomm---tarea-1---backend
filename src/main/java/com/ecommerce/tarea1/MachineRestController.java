package com.ecommerce.tarea1;

import com.google.gson.Gson;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/rs")
@CrossOrigin
public class MachineRestController {

    @GetMapping("/component")
    public Object randomMachineStatus(@RequestParam String name, @RequestParam String date) {
        return getHardwareInfo(name);
    }

    public Float randomValue() {
        Random r = new Random();
        float max = 1;
        float min = 0;
        return r.nextFloat() * (max - min) + min;
    }

    public Object getHardwareInfo(String name) {
        Map<String, String> Map = new HashMap<String, String>();
        Map.put("name", name);
        Map.put("disponibility", String.valueOf(randomValue()));
        Gson json = new Gson();
        return json.toJson(Map);
    }
}
